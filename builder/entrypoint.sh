#!/bin/bash
WORKSPACE="$HOME/workspace"
VERSION="v22.03.3"

if [ ! -d ${WORKSPACE}/openwrt ]; then
    mkdir -p "${WORKSPACE}/openwrt"
    git clone --depth 1 --branch "${VERSION}" \
        https://git.openwrt.org/openwrt/openwrt.git \
        "${WORKSPACE}/openwrt"
    cd "${WORKSPACE}/openwrt"
    git switch -c "${VERSION}"
fi

# exec runuser -u builder "$@"
exec /bin/bash
