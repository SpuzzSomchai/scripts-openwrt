#!/bin/bash
WORKSPACE="$HOME/workspace"
VERSION="22.03.3"
TARGET="ath79/generic"
TARGET_A=$(echo ${TARGET} | sed 's/\//-/')

if [ ! -d "$WORKSPACE/openwrt-imagebuilder" ]; then
    curl -o openwrt-imagebuilder.tar.xz \
    "https://downloads.openwrt.org/releases/${VERSION}/targets/${TARGET}/openwrt-imagebuilder-${VERSION}-${TARGET_A}.Linux-x86_64.tar.xz"
    tar xvf openwrt-imagebuilder.tar.xz
    cd "$WORKSPACE/openwrt-imagebuilder"
fi

# exec runuser -u builder "$@"
exec /bin/bash
