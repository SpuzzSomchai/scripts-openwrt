A collection of OpenWRT based build and operational scripts

## Print known functionality:

```sh
./build.sh -h
```

## Add additional build profiles:

- create a file with the profile name inside `.configs`
- export the setting variables:
    ```sh
      export LAN_IPADDR
      export LAN_HOSTNAME

      export AP_MODE
      
      export SSID
      export SEC_MODE
      export PSK
    ```
- add package list if you want any thing outside default
- build

