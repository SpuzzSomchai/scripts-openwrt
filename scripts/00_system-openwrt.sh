#!/bin/bash

# export LAN_HOSTNAME="\$(uci get system.@system[0].hostname)"

cat <<EOF
uci set system.@system[0].hostname="\$(cat /etc/board.json | grep name | cut -d : -f2 | awk -F\" '{print \$2}')"
uci set system.@system[0].zonename='Asia/Kolkata'
uci set system.@system[0].timezone='IST-5:30'
$(
    if [ ! -z "${LAN_HOSTNAME}" ]; then
        echo uci set network.lan.hostname="${LAN_HOSTNAME}"
    fi
)
EOF
