#!/bin/bash

# export AP_MODE

if [ -z "${AP_MODE}" ]; then
    exit 0
fi

cat <<EOF
# Set lan logical interface as bridge (to allow bridge multiple physical interfaces)
uci set network.lan.type='bridge'
if [ "\$(uci -q show network.wan.device)" != "" ]; then
    # assign WAN physical interface to LAN (will be available as an additional LAN port now)
    uci set network.lan.device="\$(uci get network.lan.device) \$(uci get network.wan.device)"
    uci del network.wan.device
    # Remove wan logical interface, since we will not need it.
    uci del network.wan
fi
EOF
