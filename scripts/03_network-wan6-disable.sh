#!/bin/bash

# export AP_MODE

if [ -z "${AP_MODE}" ]; then
    exit 0
fi

cat <<EOF
uci del network.wan6
uci del_list firewall.@zone[1].network='wan6'
EOF
