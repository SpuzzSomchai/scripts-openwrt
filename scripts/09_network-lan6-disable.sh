#!/bin/bash

cat <<EOF
# DISABLE IPV6 LAN
uci del network.lan.ip6assign
uci set network.lan.delegate='0'
uci del network.globals.ula_prefix
uci del dhcp.lan.dhcpv6
uci del dhcp.lan.ra
$(
    if [ ! -z "${AP_MODE}" ]; then
        echo "uci del dhcp.odhcpd"
        echo "/etc/init.d/odhcpd disable"
        echo "/etc/init.d/odhcpd stop"
    fi
)
EOF
