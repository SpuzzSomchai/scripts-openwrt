#!/bin/bash

cat <<EOF
# ENABLE SLAAC
uci set dhcp.lan.ra='server'
uci set dhcp.lan.ra_flags='none'
EOF
