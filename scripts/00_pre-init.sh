#!/bin/bash

cat <<EOF
#!/bin/bash

if [ "\$(uci -q get system.@system[0].init)" = "" ]; then
  uci set system.@system[0].init='initiated'
  uci commit
else
  exit 0
fi

if [ -e /etc/init ]; then
  exit 0
else
  touch /etc/init
fi
EOF
