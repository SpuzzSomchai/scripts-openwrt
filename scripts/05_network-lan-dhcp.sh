#!/bin/bash

# export AP_MODE

if [ -z "${AP_MODE}" ]; then
  exit 0
fi

cat <<EOF
# Disable Dnsmasq completely (it is important to commit or discard dhcp)
uci commit dhcp; echo '' > /etc/config/dhcp
/etc/init.d/dnsmasq disable
/etc/init.d/dnsmasq stop

# Set DHCP on LAN (not recommended, but useful when Dumb AP is moveable from one building to another)
uci del network.lan.broadcast
uci del network.lan.dns
uci del network.lan.gateway
uci del network.lan.ipaddr
uci del network.lan.netmask
uci set network.lan.proto='dhcp'
EOF
