#!/bin/bash

# export LAN_IPADDR
# export LAN_HOSTNAME

cat <<EOF
# IPV4 LAN DEFAULTS
uci set dhcp.@dnsmasq[0].local='/local/'
uci set dhcp.@dnsmasq[0].domain='local'
$(
    # if [ ! -z "${LAN_HOSTNAME}" ]; then
    #     echo uci add_list dhcp.@dnsmasq[0].address="/${LAN_HOSTNAME}/\$(uci -q show network.lan.ipaddr | awk -F= '{ print \$2 }' | awk -F\' '{print \$2}')"
    # fi
)
$(
    if [ ! -z "${LAN_IPADDR}" ]; then
        echo uci set network.lan.ipaddr="${LAN_IPADDR}"
    fi
)
EOF
# uci set network.lan.broadcast=\$(uci -q show network.lan.ipaddr | awk -F= '{ print \$2 }' | awk -F. '{print \$1 "." \$2 "." \$3 ".255"}')
