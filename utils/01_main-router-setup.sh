#!/bin/bash

# System configuration
# uci set system.@system[0].hostname='$(cat /etc/board.json | grep "name" | cut -d : -f2 | awk -F\" "{print $2}")'
uci set system.@system[0].hostname="$(cat /etc/board.json | grep name | cut -d : -f2 | awk -F\" '{print $2}')"
uci set system.@system[0].hostname=''
uci set network.lan.hostname='$(uci get system.@system[0].hostname)'
uci set system.@system[0].zonename='Asia/Kolkata'
uci set system.@system[0].timezone='IST-5:30'

# Administration configuration
passwd

# Set static network configuration (sample config for 192.168.10.0/24)
# 192.168.10.1 is the Main Router
uci set network.lan.ipaddr='192.168.10.1'
uci set network.lan.broadcast='192.168.10.255'
uci set dhcp.@dnsmasq[0].local='/local/'
uci set dhcp.@dnsmasq[0].domain='local'

# ========================================================
# Optional, Disable LAN IPv6
# ========================================================
uci del network.lan.ip6assign
uci set network.lan.delegate='0'
# uci del network.globals.ula_prefix # fda2:1f39:4377::/48
uci del dhcp.lan.dhcpv6
uci del dhcp.lan.ra
uci del dhcp.odhcpd
/etc/init.d/odhcpd disable
/etc/init.d/odhcpd stop

# ========================================================
# Optional, Disable WAN IPv6
# ========================================================
uci del network.wan6
uci del_list firewall.@zone[1].network='wan6'

# ========================================================
# Commit changes, flush, and restart network
# ========================================================
uci commit
sync
/etc/init.d/network restart
# If all is OK then reboot and test again:
reboot
