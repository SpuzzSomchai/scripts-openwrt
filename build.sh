#!/bin/bash
set -e

# export IMAGEBUILDER_BIN

RELEASE="21.02.7"
TARGET="sunxi-cortexa53"
PROFILE="pine64_pine64-plus"
FILES="$PWD/files"

usage() {
    cat <<-HELP
usage: $0 [options]
  -T IMAGEBUILDER_BIN       path to imagebuilder ($IMAGEBUILDER_BIN)
  -t TARGET                 build for a specific target ($TARGET)
  -p PROFILE                build a specific profile ($PROFILE)
  -P PACKAGES               include packages in firmware ($PACKAGES)
  -l                        only list availble profiles for target
  -d DRY_RUN                output defaults file only
HELP
}

[[ $1 == --help ]] && {
    usage
    exit 0
}
while getopts "hr:T:t:p:P::ld" opt; do
    case "$opt" in
    T) IMAGEBUILDER_BIN=$OPTARG ;;
    t) TARGET=$OPTARG ;;
    p) PROFILE=$OPTARG ;;
    P) PACKAGES=$OPTARG ;;
    l) LISTONLY=yes ;;
    d) DRY_RUN=yes ;;
    h)
        usage
        exit 0
        ;;
    \?) exit 1 ;;
    esac
done

if [ -z "${IMAGEBUILDER_BIN}" ]; then
    IMAGEBUILDER_BIN="$PWD/openwrt-imagebuilder-$RELEASE-$TARGET.Linux-x86_64"
fi

if [[ $DRY_RUN != yes ]]; then
    if [ ! -d "${IMAGEBUILDER_BIN}" ]; then
        echo "ERROR: PROVIDE IMAGEBUILDER_BIN"
        exit 1
    fi

    if [[ $LISTONLY == yes ]]; then
        make --directory="${IMAGEBUILDER_BIN}" info
        exit 0
    fi
fi

if [ ! -f "./configs/$PROFILE" ]; then
    echo "ERROR: CONFIGURATION NOT SUPPORTED"
    exit 1
fi

source ./configs/$PROFILE

CONFIG_FILE="$FILES/etc/uci-defaults"
rm -rf $CONFIG_FILE
mkdir -p $CONFIG_FILE

CONFIG_FILE="$CONFIG_FILE/99-uci-defaults"
touch $CONFIG_FILE
chmod 755 $CONFIG_FILE

tee $CONFIG_FILE <<EOF
$(sh ./scripts/00_pre-init.sh)
$(sh ./scripts/00_system-openwrt.sh)
$(sh ./scripts/01_wlan-factory.sh)
$(sh ./scripts/02_network-switch.sh)
$(sh ./scripts/03_network-wan6-disable.sh)
$(sh ./scripts/04_network-lan-static.sh)
$(sh ./scripts/05_network-lan-dhcp.sh)
$(sh ./scripts/09_network-lan6-disable.sh)
$(sh ./scripts/09_network-lan6-slaac.sh)
$(sh ./scripts/99_post-init.sh)
EOF

PKGS=$(echo ${CONFIG_PACKAGES[@]})

if [[ $DRY_RUN == yes ]]; then
    make --directory="${IMAGEBUILDER_BIN}" manifest \
        PROFILE="$PROFILE" \
        FILES="$FILES" \
        PACKAGES="${PKGS} ${PACKAGES}"
    exit 0
fi

make --directory="${IMAGEBUILDER_BIN}" image \
    PROFILE="$PROFILE" \
    FILES="$FILES" \
    PACKAGES="${PKGS} ${PACKAGES}"
